//
//  UILoginController.swift
//  MSExtensions
//
//  Created by Yaser Almasri on 3/1/17.
//  Copyright © 2017 Yaser Almasri. All rights reserved.
//

import UIKit
import LocalAuthentication

open class UILoginController: UIViewController {
    
    public var touchIdAuthentication: Bool = false
    public var statusBarStyle: UIStatusBarStyle = UIStatusBarStyle.default
    
    var keyboardShown: Bool = false
    var logoImageViewCenter: [NSLayoutConstraint]?
    var emailTextFieldAnchor: [NSLayoutConstraint]?
    var passwordTextFieldAnchor: [NSLayoutConstraint]?
    
    public let logoImageView: UIImageView = {
        let image = UIImage(named: "logo")
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    public lazy var emailTextField: UIDarkTextField = {
        let tf = UIDarkTextField()
        tf.customPlaceholder = "Enter email"
        tf.layer.borderColor = UIColor(red: 141/255, green: 141/255, blue: 141/255, alpha: 1).cgColor
        tf.layer.borderWidth = 1
        tf.keyboardType = .emailAddress
        tf.autocapitalizationType = .none
        tf.returnKeyType = .next
        tf.delegate = self
        tf.tag = 0
        return tf
    }()
    
    public lazy var passwordTextField: UIDarkTextField = {
        let tf = UIDarkTextField()
        tf.customPlaceholder = "Enter password"
        tf.layer.borderColor = UIColor(red: 141/255, green: 141/255, blue: 141/255, alpha: 1).cgColor
        tf.layer.borderWidth = 1
        tf.isSecureTextEntry = true
        tf.returnKeyType = .go
        tf.delegate = self
        tf.tag = 1
        return tf
    }()
    
    public lazy var loginButton: CustomButton = {
        let btn = CustomButton()
        
        btn.borderColor = UIColor(red: 141/255, green: 141/255, blue: 141/255, alpha: 1)
        btn.setTitle("Log in", for: .normal)
        btn.setTitleColor(btn.borderColor, for: .normal)
        btn.setTitleColor(UIColor.Custom.primary, for: .highlighted)
        btn.addTarget(self, action: #selector(handleLogin), for: .touchUpInside)
        btn.setImage(UIImage.imageIcon(icon: .signIn, color: btn.borderColor, size: 30), for: .normal)
        btn.setImage(UIImage.imageIcon(icon: .signIn, color: UIColor.Custom.primary, size: 30), for: UIControlState.highlighted)
        
        return btn
    }()
    
    public let touchIDButton: CheckBox = {
        let btn = CheckBox()
        btn.isChecked = UserDefaults.standard.touchID()
        btn.setTitle("Activate login with TouchID", for: .normal)
        btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
        btn.addTarget(btn, action: #selector(btn.buttonClicked), for: .touchUpInside)
        
        return btn
    }()
    
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return self.statusBarStyle
    }
    
    override open func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if touchIdAuthentication {
            self.authenticateUser()
        }
        
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .black
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override open func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.emailTextField.resignFirstResponder()
        self.passwordTextField.resignFirstResponder()
        super.touchesBegan(touches, with: event)
    }
    
}

extension UILoginController {
    
    @objc func keyboardWillShow(notification:NSNotification) {
        let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardFrame: NSValue = userInfo.value(forKey: UIKeyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.cgRectValue
        let keyboardHeight = keyboardRectangle.height
        
        if self.keyboardShown == false {
            self.keyboardShown = true
            self.logoImageViewCenter?[1].constant -= keyboardHeight
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        }
        
    }
    
    @objc func keyboardWillHide(notification:NSNotification) {
        self.keyboardShown = false
        self.logoImageViewCenter?[1].constant = 0
        UIView.animate(withDuration: 10) {
            self.view.layoutIfNeeded()
        }
    }
    
}

extension UILoginController: UITextFieldDelegate {
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailTextField {
            self.emailTextField.resignFirstResponder()
            self.passwordTextField.becomeFirstResponder()
        }
        else if textField == passwordTextField {
            self.passwordTextField.resignFirstResponder()
            self.handleLogin()
        }
        return true
    }
    
}

extension UILoginController {
    
    @objc open func handleLogin() {
        self.emailTextField.resignFirstResponder()
        self.passwordTextField.resignFirstResponder()
    }
    
    public func setupLoginForm() {
        
        self.view.addSubview(logoImageView)
        self.view.addSubview(emailTextField)
        self.view.addSubview(passwordTextField)
        self.view.addSubview(loginButton)
        
        self.logoImageView.widthAnchor.constraint(equalToConstant: 260).isActive = true
        self.logoImageView.heightAnchor.constraint(equalToConstant: 160).isActive = true
        self.logoImageViewCenter = self.logoImageView.centerTo(view: self.view)
        
        self.emailTextFieldAnchor = emailTextField.anchor(
            logoImageView.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor,
            topConstant: 16, leftConstant: 32, bottomConstant: 0, rightConstant: 32,
            widthConstant: 0, heightConstant: 40
        )
        
        self.passwordTextFieldAnchor = passwordTextField.anchor(
            emailTextField.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor,
            topConstant: 16, leftConstant: 32, bottomConstant: 0, rightConstant: 32,
            widthConstant: 0, heightConstant: 40
        )
        
        _ = loginButton.anchor(
            passwordTextField.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor,
            topConstant: 16, leftConstant: 32, bottomConstant: 0, rightConstant: 32,
            widthConstant: 0, heightConstant: 40
        )
        
        if touchIdAuthentication {
            view.addSubview(touchIDButton)
            _ = touchIDButton.anchor(
                loginButton.bottomAnchor, left: loginButton.leftAnchor, bottom: nil, right: loginButton.rightAnchor,
                topConstant: 16, leftConstant: 0, bottomConstant: 0, rightConstant: 0,
                widthConstant: 0, heightConstant: 40
            )
        }
        
    }
    
    func authenticateUser() {
        guard UserDefaults.standard.touchID() else {
            return
        }
        
        let context = LAContext()
        
        var error: NSError?
        
        let reasonString = "Authentication is needed to access."
        
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            _ = [context .evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reasonString, reply: { (success, evalPolicyError) -> Void in
                if success {
                    self.emailTextField.text = UserDefaults.standard.userEmail()
                    self.passwordTextField.text = UserDefaults.standard.userPass()
                    self.handleLogin()
                }
                else{
                    // print(evalPolicyError?.localizedDescription)
                    
                    switch evalPolicyError!._code {
                        
                    case LAError.systemCancel.rawValue:
                        print("Authentication was cancelled by the system")
                        
                    case LAError.userCancel.rawValue:
                        print("Authentication was cancelled by the user")
                        
                    case LAError.userFallback.rawValue:
                        print("User selected to enter custom password")
                        // Enter password manually
                        
                    default:
                        print("Authentication failed")
                        // Enter password manually
                    }
                }
            })]
        }
        else {
            switch error!.code{
                
            case LAError.touchIDNotEnrolled.rawValue:
                print("TouchID is not enrolled")
                
            case LAError.passcodeNotSet.rawValue:
                print("A passcode has not been set")
                
            default:
                print("TouchID not available")
            }
            
            // print(error?.localizedDescription)
            // Enter password manually
        }
    }

}






