//
//  UISeparatorView.swift
//  MSExtensions
//
//  Created by Yaser Almasri on 4/1/17.
//  Copyright © 2017 Yaser Almasri. All rights reserved.
//

import UIKit

class UISeparatorView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        self.backgroundColor = UIColor(red: 0.783922, green: 0.780392, blue: 0.8, alpha: 1)
    }
    
}
