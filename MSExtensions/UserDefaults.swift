//
//  UserDefaults.swift
//  MSExtensions
//
//  Created by Yaser Almasri on 3/1/17.
//  Copyright © 2017 Yaser Almasri. All rights reserved.
//

import UIKit

extension UserDefaults {
    
    enum UserDefaultsKeys: String {
        case isLoggedIn
        case apiUrl
        case userToken
        case userEmail
        case userPass
        case touchID
    }
    
    public func setIsLoggedIn(value: Bool) {
        set(value, forKey: UserDefaultsKeys.isLoggedIn.rawValue)
    }
    
    public func isLoggedIn() -> Bool {
        return bool(forKey: UserDefaultsKeys.isLoggedIn.rawValue)
    }
    
    public func setUserToken(value: String) {
        set(value, forKey: UserDefaultsKeys.userToken.rawValue)
    }
    
    public func userToken() -> String {
        return string(forKey: UserDefaultsKeys.userToken.rawValue)!
    }
    
    public func setUserEmail(value: String) {
        set(value, forKey: UserDefaultsKeys.userEmail.rawValue)
    }
    
    public func userEmail() -> String {
        return string(forKey: UserDefaultsKeys.userEmail.rawValue)!
    }
    
    public func setUserPass(value: String) {
        set(value, forKey: UserDefaultsKeys.userPass.rawValue)
        setTouchID(value: true)
    }
    
    public func userPass() -> String {
        return string(forKey: UserDefaultsKeys.userPass.rawValue)!
    }
    
    public func setTouchID(value: Bool) {
        set(value, forKey: UserDefaultsKeys.touchID.rawValue)
    }
    
    public func touchID() -> Bool {
        return bool(forKey: UserDefaultsKeys.touchID.rawValue)
    }
    
}
