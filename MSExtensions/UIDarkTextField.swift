//
//  DarkTextField.swift
//  MSExtensions
//
//  Created by Yaser Almasri on 3/1/17.
//  Copyright © 2017 Yaser Almasri. All rights reserved.
//

import UIKit

public class UIDarkTextField: UIPaddedTextField {
    
    public var customPlaceholder: String? {
        didSet {
            self.attributedPlaceholder = NSAttributedString(
                string: customPlaceholder! as String,
                attributes: [
                    NSAttributedStringKey.foregroundColor: UIColor.lightGray
                ]
            )
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.layer.cornerRadius = 5.0;
        self.layer.borderColor = UIColor.gray.cgColor
        self.layer.borderWidth = 1.5
        self.textColor = .white
        self.tintColor = .purple
        
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
