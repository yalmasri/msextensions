//
//  String.swift
//  MSExtensions
//
//  Created by Yaser Almasri on 3/1/17.
//  Copyright © 2017 Yaser Almasri. All rights reserved.
//

import UIKit

extension String {
    
    public func sizeFor() -> CGRect {
        let size = CGSize(width: 200, height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        return NSString(string: self).boundingRect(
            with: size,
            options: options,
//            attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 12)],
            attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 12)],
            context: nil
        )
        
    }
    
}
