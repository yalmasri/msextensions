//
//  PaddedTextFieldCell.swift
//  MSExtensions
//
//  Created by Yaser Almasri on 3/1/17.
//  Copyright © 2017 Yaser Almasri. All rights reserved.
//

import UIKit

public class PaddedTextFieldCell: UITableViewCell {
    
    let textField: UIPaddedTextField = {
        let tf = UIPaddedTextField()
        tf.placeholder = ""
        tf.font = tf.font?.withSize(15)
        return tf
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        
        setupViews()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        addSubview(textField)
        _ = textField.anchor(
            topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor,
            topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0,
            widthConstant: 0, heightConstant: 0
        )
    }
}
