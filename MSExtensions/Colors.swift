//
//  Colors.swift
//  MSExtensions
//
//  Created by Yaser Almasri on 3/1/17.
//  Copyright © 2017 Yaser Almasri. All rights reserved.
//

import UIKit

extension UIColor {

    public class Custom {
        private static var primaryColor: UIColor = UIColor(red: 101/255, green: 160/255, blue: 214/255,  alpha: 1)
        private static var secondaryColor: UIColor = UIColor(red: 8/255,   green: 44/255,  blue: 74/255,   alpha: 1)

        public static var primary: UIColor {
            get { return self.primaryColor }
            set (color) { self.primaryColor = color }
        }

        public static var secondary: UIColor {
            get { return self.secondaryColor }
            set (color) { self.secondaryColor = color }
        }

        private static var dangerColor: UIColor = UIColor(red: 217/255, green: 83/255,  blue: 79/255,   alpha: 1)
        private static var warningColor: UIColor = UIColor(red: 240/255, green: 173/255, blue: 78/255,   alpha: 1)
        private static var successColor: UIColor = UIColor(red: 92/255,  green: 184/255, blue: 92/255,   alpha: 1)
        private static var infoColor: UIColor = UIColor(red: 0/255,   green: 192/255, blue: 239/255,  alpha: 1)

        private static var dangerAlphaColor: UIColor = UIColor(red: 217/255, green: 83/255,  blue: 79/255,   alpha: 0.2)
        private static var warningAlphaColor: UIColor = UIColor(red: 240/255, green: 173/255, blue: 78/255,   alpha: 0.2)
        private static var successAlphaColor: UIColor = UIColor(red: 92/255,  green: 184/255, blue: 92/255,   alpha: 0.2)
        private static var infoAlphaColor: UIColor = UIColor(red: 0/255,   green: 192/255, blue: 239/255,  alpha: 0.2)

        public static var danger: UIColor {
            get { return self.dangerColor }
            set (color) { self.dangerColor = color }
        }
        public static var warning: UIColor {
            get { return self.warningColor }
            set (color) { self.warningColor = color }
        }
        public static var success: UIColor {
            get { return self.successColor }
            set (color) { self.successColor = color }
        }
        public static var info: UIColor {
            get { return self.infoColor }
            set (color) { self.infoColor = color }
        }

        public static var dangerAlpha: UIColor {
            get { return self.dangerAlphaColor }
            set (color) { self.dangerAlphaColor = color }
        }
        public static var warningAlpha: UIColor {
            get { return self.warningAlphaColor }
            set (color) { self.warningAlphaColor = color }
        }
        public static var successAlpha: UIColor {
            get { return self.successAlphaColor }
            set (color) { self.successAlphaColor = color }
        }
        public static var infoAlpha: UIColor {
            get { return self.infoAlphaColor }
            set (color) { self.infoAlphaColor = color }
        }

    }

    public static var danger: UIColor {
        get {
            print("WARNING!: UIColor.danger is deprecated, please use: UIColor.Custom.danger")
            return UIColor.Custom.danger
        }
    }
    public static var warning: UIColor {
        get {
            print("WARNING!: UIColor.warning is deprecated, please use: UIColor.Custom.warning")
            return UIColor.Custom.warning
        }
    }
    public static var success: UIColor {
        get {
            print("WARNING!: UIColor.success is deprecated, please use: UIColor.Custom.success")
            return UIColor.Custom.success
        }
    }
    public static var info: UIColor {
        get {
            print("WARNING!: UIColor.info is deprecated, please use: UIColor.Custom.info")
            return UIColor.Custom.info
        }
    }

    public static var dangerAlpha: UIColor {
        get {
            print("WARNING!: UIColor.dangerAlpha is deprecated, please use: UIColor.Custom.dangerAlpha")
            return UIColor.Custom.dangerAlpha
        }
    }
    public static var warningAlpha: UIColor {
        get {
            print("WARNING!: UIColor.warningAlpha is deprecated, please use: UIColor.Custom.warningAlpha")
            return UIColor.Custom.warningAlpha
        }
    }
    public static var successAlpha: UIColor {
        get {
            print("WARNING!: UIColor.successAlpha is deprecated, please use: UIColor.Custom.successAlpha")
            return UIColor.Custom.successAlpha
        }
    }
    public static var infoAlpha: UIColor {
        get {
            print("WARNING!: UIColor.infoAlpha is deprecated, please use: UIColor.Custom.infoAlpha")
            return UIColor.Custom.infoAlpha
        }
    }

}


















