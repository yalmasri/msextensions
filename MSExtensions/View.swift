//
//  View.swift
//  MSExtensions
//
//  Created by Yaser Almasri on 3/1/17.
//  Copyright © 2017 Yaser Almasri. All rights reserved.
//

import UIKit

extension UIView {
    
    public func anchor(
        top: NSLayoutYAxisAnchor? = nil, left: NSLayoutXAxisAnchor? = nil, bottom: NSLayoutYAxisAnchor? = nil, right: NSLayoutXAxisAnchor? = nil,
        topConstant: CGFloat = 0, leftConstant: CGFloat = 0, bottomConstant: CGFloat = 0, rightConstant: CGFloat = 0,
        widthConstant: CGFloat = 0, heightConstant: CGFloat = 0
        ) {
        _ = self.anchor(
            top, left: left, bottom: bottom, right: right,
            topConstant: topConstant, leftConstant: leftConstant, bottomConstant: bottomConstant, rightConstant: rightConstant,
            widthConstant: widthConstant, heightConstant: heightConstant
        )
    }
    
    public func anchor(
        _ top: NSLayoutYAxisAnchor? = nil, left: NSLayoutXAxisAnchor? = nil, bottom: NSLayoutYAxisAnchor? = nil, right: NSLayoutXAxisAnchor? = nil,
        topConstant: CGFloat = 0, leftConstant: CGFloat = 0, bottomConstant: CGFloat = 0, rightConstant: CGFloat = 0,
        widthConstant: CGFloat = 0, heightConstant: CGFloat = 0
        ) -> [NSLayoutConstraint] {
        
        translatesAutoresizingMaskIntoConstraints = false
        
        var anchors = [NSLayoutConstraint]()
        
        if let top = top {
            anchors.append(topAnchor.constraint(equalTo: top, constant: topConstant))
        }
        
        if let left = left {
            anchors.append(leftAnchor.constraint(equalTo: left, constant: leftConstant))
        }
        
        if let bottom = bottom {
            anchors.append(bottomAnchor.constraint(equalTo: bottom, constant: -bottomConstant))
        }
        
        if let right = right {
            anchors.append(rightAnchor.constraint(equalTo: right, constant: -rightConstant))
        }
        
        if widthConstant > 0 {
            anchors.append(widthAnchor.constraint(equalToConstant: widthConstant))
        }
        
        if heightConstant > 0 {
            anchors.append(heightAnchor.constraint(equalToConstant: heightConstant))
        }
        
        anchors.forEach({$0.isActive = true})
        
        return anchors
    }
    
    public func centerTo(view: UIView) -> [NSLayoutConstraint] {
        
        let xAnchor = self.centerX(view: view)
        let yAnchor = self.centerY(view: view)
        
        return [xAnchor, yAnchor]
    }
    
    public func centerX(view: UIView, constant: CGFloat = 0) -> NSLayoutConstraint {
        
        translatesAutoresizingMaskIntoConstraints = false
        let anchor = centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: constant)
        anchor.isActive = true
        
        return anchor
    }
    
    public func centerY(view: UIView, constant: CGFloat = 0) -> NSLayoutConstraint {
        
        translatesAutoresizingMaskIntoConstraints = false
        let anchor = centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: constant)
        anchor.isActive = true

        return anchor
    }
    
    public func rotate360Degrees(duration: CFTimeInterval = 1.0, completionDelegate: AnyObject? = nil) {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(Double.pi * 2.0)
        rotateAnimation.duration = duration
        
        if let delegate = completionDelegate as! CAAnimationDelegate? {
            rotateAnimation.delegate = delegate
        }
        self.layer.add(rotateAnimation, forKey: nil)
    }
    
    public func setWidth(constant: CGFloat) -> NSLayoutConstraint {
        
        translatesAutoresizingMaskIntoConstraints = false
        let anchor = self.widthAnchor.constraint(equalToConstant: constant)
        anchor.isActive = true
        
        return anchor
    }
    
    public func setHeight(constant: CGFloat) -> NSLayoutConstraint {
        
        translatesAutoresizingMaskIntoConstraints = false
        let anchor = self.heightAnchor.constraint(equalToConstant: constant)
        anchor.isActive = true
        
        return anchor
    }
    
    public func setTopAnchorTo (view: NSLayoutYAxisAnchor, constant: CGFloat) {
        let anchor = self.topAnchor.constraint(equalTo: view, constant: constant)
        anchor.isActive = true
    }
    
    public func setBottomAnchorTo (view: NSLayoutYAxisAnchor, constant: CGFloat) {
        let anchor = self.bottomAnchor.constraint(equalTo: view, constant: -constant)
        anchor.isActive = true
    }
    
}





















