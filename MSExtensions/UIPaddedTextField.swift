//
//  PaddedTextField.swift
//  MSExtensions
//
//  Created by Yaser Almasri on 3/1/17.
//  Copyright © 2017 Yaser Almasri. All rights reserved.
//

import UIKit

public class UIPaddedTextField: UITextField {
    
    public var name: String?
    
    override public func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 10, y: bounds.origin.y, width: bounds.width - 20, height: bounds.height)
    }
    
    override public func editingRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 10, y: bounds.origin.y, width: bounds.width - 20, height: bounds.height)
    }
    
}
