//
//  CheckBox.swift
//  MSExtensions
//
//  Created by Yaser Almasri on 3/1/17.
//  Copyright © 2017 Yaser Almasri. All rights reserved.
//

import UIKit

public class CheckBox: UIButton {
    
    public var checkedImage:    UIImage = UIImage.imageIcon(icon: .check, color: UIColor.Custom.primary, size: 30)
    public var uncheckedImage:  UIImage = UIImage.imageIcon(icon: .times, color: .gray, size: 30)
    
    public var isChecked: Bool = false {
        didSet{
            if isChecked == true {
                self.setImage(self.checkedImage, for: .normal)
            } else {
                self.setImage(self.uncheckedImage, for: .normal)
            }
        }
    }
    
    override public func awakeFromNib() {
        self.isChecked = false
    }
    
    @objc func buttonClicked(sender: UIButton) {
        if sender == self {
            isChecked = !isChecked
        }
    }
}
