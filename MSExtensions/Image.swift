//
//  Image.swift
//  MSExtensions
//
//  Created by Yaser Almasri on 3/1/17.
//  Copyright © 2017 Yaser Almasri. All rights reserved.
//

import UIKit
import FontAwesome_swift

extension UIImage {
    public static func imageIcon(icon: FontAwesome, color: UIColor, size: CGFloat) -> UIImage {
        return UIImage.fontAwesomeIcon(
            name: icon,
            textColor: color,
            size: CGSize(width: size, height: size)
            ).withRenderingMode(.alwaysOriginal)
    }
    
    public static func imageIconWithBackground(icon: FontAwesome, color: UIColor, size: CGFloat, backgroundColor: UIColor) -> UIImage {
        return UIImage.fontAwesomeIcon(
            name: icon, textColor: color, size: CGSize(width: size, height: size), backgroundColor: backgroundColor
            ).withRenderingMode(.alwaysOriginal)
    }
    
    class func imageWithColor(color: UIColor, size: CGSize) -> UIImage {
        let rect: CGRect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}
