//
//  RadioButton.swift
//  MSExtensions
//
//  Created by Yaser Almasri on 3/1/17.
//  Copyright © 2017 Yaser Almasri. All rights reserved.
//

import UIKit

public class RadioButton: UIButton {
    var alternateButton:Array<RadioButton>?
    
    override public func awakeFromNib() {
        self.layer.cornerRadius = 5
        self.layer.borderWidth = 2.0
        self.layer.masksToBounds = true
    }
    
    func unselectAlternateButtons(){
        if alternateButton != nil {
            self.isSelected = true
            
            for aButton:RadioButton in alternateButton! {
                aButton.isSelected = false
            }
        }else{
            toggleButton()
        }
    }
    
    override public func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        unselectAlternateButtons()
        super.touchesBegan(touches, with: event)
    }
    
    //    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
    //        unselectAlternateButtons()
    //        super.touchesBegan(touches, with: event)
    //    }
    
    func toggleButton(){
        self.isSelected = !isSelected
    }
    
    override public var isSelected: Bool {
        didSet {
            if isSelected {
                self.layer.borderColor = UIColor.blue.cgColor
            } else {
                self.layer.borderColor = UIColor.gray.cgColor
            }
        }
    }
}
