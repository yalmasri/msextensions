//
//  Number.swift
//  MSExtensions
//
//  Created by Yaser Almasri on 3/1/17.
//  Copyright © 2017 Yaser Almasri. All rights reserved.
//

import UIKit

extension NSNumber {
    
    public func toCurrency(locale: String = "en_us") -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        
        formatter.locale = Locale(identifier: locale)
        return formatter.string(from: self)!
    }
}
