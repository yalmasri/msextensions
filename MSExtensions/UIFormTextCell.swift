//
//  FormTextCell.swift
//  MSExtensions
//
//  Created by Yaser Almasri on 3/1/17.
//  Copyright © 2017 Yaser Almasri. All rights reserved.
//

import UIKit

public class UIFormTextCell: UITableViewCell {
    
    public let textField: UIFormTextField = {
        let tf = UIFormTextField()
        tf.autocapitalizationType = .words
        tf.keyboardType = UIKeyboardType.default
        tf.clearButtonMode = .whileEditing
        tf.font = UIFont.systemFont(ofSize: 14)
        return tf
    }()
    
    public init(
        autocapitalizationType: UITextAutocapitalizationType = .words,
        keybordType: UIKeyboardType = .default
        ) {
        super.init(style: .subtitle, reuseIdentifier: "")
        
        self.textField.autocapitalizationType = autocapitalizationType
        self.textField.keyboardType = keybordType
        
        self.selectionStyle = .none
        self.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.8)
        
        setupViews()
        
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        
        self.addSubview(textField)
        _ = self.textField.anchor(
            self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor,
            topConstant: 2, leftConstant: 15, bottomConstant: 2, rightConstant: 2,
            widthConstant: 0, heightConstant: 0
        )
        
    }
    
}
