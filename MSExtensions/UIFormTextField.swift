//
//  FormTextField.swift
//  MSExtensions
//
//  Created by Yaser Almasri on 3/1/17.
//  Copyright © 2017 Yaser Almasri. All rights reserved.
//

// Credits: JiroTextField

import UIKit

public class UIFormTextField: UITextField {
    
    let PLACEHOLDER_LABEL_Y: CGFloat = 12
    var isPlaceholderLabelVisible: Bool = false
    
    var placeholderLabelYConstraint: NSLayoutConstraint?
    
    override open var placeholder: String? {
        didSet {
            self.placeholderLabel.text = placeholder
        }
    }
    
    override open var text: String? {
        didSet {
            if text != "" && !isPlaceholderLabelVisible {
                self.isPlaceholderLabelVisible = true
                if placeholderLabelYConstraint != nil {
                    self.placeholderLabelYConstraint?.constant = -12
                }
                self.layoutIfNeeded()
                self.placeholderLabel.alpha = 1
            }
        }
    }
    
    let placeholderLabel: UILabel = {
        let label = UILabel()
        label.alpha = 0
        label.textColor = UIColor.Custom.primary
        label.font = UIFont.systemFont(ofSize: 10)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.font = UIFont.systemFont(ofSize: 14)
        
        self.addSubview(placeholderLabel)
        placeholderLabelYConstraint = self.placeholderLabel.centerY(view: self)
        self.placeholderLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10).isActive = true
        self.placeholderLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        if self.text != "" {
            return CGRect(x: bounds.origin.x + 10, y: bounds.origin.y + 5, width: bounds.width - 20, height: bounds.height)
        }
        return CGRect(x: bounds.origin.x + 10, y: bounds.origin.y, width: bounds.width - 20, height: bounds.height)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        if self.text != "" {
            return CGRect(x: bounds.origin.x + 10, y: bounds.origin.y + 5, width: bounds.width - 20, height: bounds.height)
        }
        return CGRect(x: bounds.origin.x + 10, y: bounds.origin.y, width: bounds.width - 20, height: bounds.height)
    }
    
    override open func willMove(toSuperview newSuperview: UIView!) {
        if newSuperview != nil {
            NotificationCenter.default.addObserver(self, selector: #selector(textFieldTextDidChange), name: NSNotification.Name.UITextFieldTextDidChange, object: self)
        } else {
            NotificationCenter.default.removeObserver(self)
        }
    }
    
    @objc func textFieldTextDidChange() {
        if self.text == "" && isPlaceholderLabelVisible {
            self.hidePlaceholderLabel()
        } else if self.text != "" && !isPlaceholderLabelVisible {
            self.showPlaceholderLabel()
        }
    }
    
    // MARK: Animations
    
    private func showPlaceholderLabel() {
        self.isPlaceholderLabelVisible = true
        if placeholderLabelYConstraint != nil {
            self.placeholderLabelYConstraint?.constant = -12
        }
        UIView.animate(withDuration: 0.4, animations: {
            self.layoutIfNeeded()
            self.placeholderLabel.alpha = 1
        })
    }
    
    private func hidePlaceholderLabel() {
        self.isPlaceholderLabelVisible = false
        if placeholderLabelYConstraint != nil {
            self.placeholderLabelYConstraint?.constant = 0
        }
        UIView.animate(withDuration: 0.4, animations: {
            self.layoutIfNeeded()
            self.placeholderLabel.alpha = 0
        })
    }
    
}
