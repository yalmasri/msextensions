//
//  CustomCell.swift
//  Groceries
//
//  Created by Yaser Almasri on 5/28/18.
//  Copyright © 2018 Yaser Almasri. All rights reserved.
//

import UIKit

public enum CellStyle: String {
    case basic, textField, textView
}

public class MSTableViewCell: UITableViewCell {
    
    public var style: CellStyle = .basic {
        didSet {
            switch self.style {
            case .textField:
                self.textField.isHidden = false
                self.textView.isHidden = true
            case .textView:
                self.textField.isHidden = true
                self.textView.isHidden = false
            default:
                self.textField.isHidden = true
                self.textView.isHidden = true
            }
        }
    }
    
    public let textField: UITextField = {
        let tf = UITextField()
        tf.autocapitalizationType = .words
        tf.borderStyle = .roundedRect
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.isHidden = true
        tf.clearButtonMode = .whileEditing
        return tf
    }()
    
    public let textView: UITextView = {
        let tv = UITextView()
        tv.autocapitalizationType = .words
        tv.font = UIFont.systemFont(ofSize: 14)
        tv.isHidden = true
        return tv
    }()
    
    public override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        self.backgroundColor = .clear
        self.setup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup () {
        self.addSubview(self.textField)
        self.addSubview(self.textView)
        
        self.textField.anchor(
            top: self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor,
            topConstant: 5, leftConstant: 10, bottomConstant: 5, rightConstant: 10,
            widthConstant: 0, heightConstant: 0
        )
        self.textView.anchor(
            top: self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor,
            topConstant: 5, leftConstant: 10, bottomConstant: 5, rightConstant: 10,
            widthConstant: 0, heightConstant: 0
        )
    }
    
}









