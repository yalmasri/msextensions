//
//  Overlay.swift
//  MSExtensions
//
//  Created by Yaser Almasri on 3/1/17.
//  Copyright © 2017 Yaser Almasri. All rights reserved.
//

import UIKit

public class Overlay: UIView {
    
    // MARK: - Class funcs
    
    public static let current = Overlay(frame: UIScreen.main.bounds)
    
    // MARK: - Instance funcs
    
    public let indicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor(white: 0, alpha: 0.2)
        indicator.frame = self.bounds
        indicator.autoresizingMask = UIViewAutoresizing.flexibleHeight
        self.addSubview(indicator)
        indicator.isUserInteractionEnabled = false
        indicator.startAnimating()
        indicator.color = UIColor.Custom.primary
        
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func show() {
        UIApplication.shared.keyWindow?.addSubview(self)
    }
    
    public func remove() {
        self.removeFromSuperview()
    }
    
}
